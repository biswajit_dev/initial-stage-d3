import React, { useEffect } from "react";
import * as d3 from "d3";

const Barchart = () => {
  useEffect(() => {
    drawChart();
  }, []);
  const drawChart = () => {
    const data = [
        {
            id: 1, region: "North", sales: 25
        },
        {
            id: 2, region: "South", sales: 19
        },
        {
            id: 3, region: "West", sales: 17.9
        },
        {
            id: 4, region: "East", sales: 8.5
        },
    ];

    const svg = d3
      .select("body")
      .append("svg")
      .attr("width", 500)
      .attr("height", 300);

    svg
      .selectAll("rect")
      .data(data)
      .enter()
      .append("rect")
      .attr("x", (d, i) => i * 70)
      .attr("y", ({sales})=> 300 - (10 * sales))
      .attr("width", 50)
      .attr("height", ({sales}) => `${sales}rem`)
      .attr("fill", "#800080");

    svg
    .selectAll("text")
    .data(data)
    .enter()
    .append("text")
    .text(({region})=> region)
    .attr("x", (d, i) => i * 70)
    .attr("y", ({sales})=> 300 - (10 * sales) - 3)
  };

  return <div id={`#root`}></div>;
};

export default Barchart;
